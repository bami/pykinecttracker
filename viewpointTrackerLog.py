#! /usr/bin/env python
''' Demo if kinect tracking controller based parallax correction
    
    Copyright 2012 by Bastian Migge <miggeb@ethz.ch>
'''

import time
from pyqtpc.Correction.Controller.Tracking import ViewpointTrackerCorrectionController
from pykinecttracker.ViewpointTrackerKinect import ViewpointTrackerKinect

def main():
    tracker = ViewpointTrackerKinect(libraryPath="./lib/libSkeletonTrackerKinect.so")
   
    trackerController = ViewpointTrackerCorrectionController(viewpointTracker=tracker,
                                                      displayOffset = 50,
                                                      pixelPitch = [0.255,0.255],
                                                      cameraPosition = [204., -80., 0.])
    while(1):
        time.sleep(1)
        for userId in range(tracker.getNumberOfUsers()):
            print "User (3d) [mm]" + str(userId) + " :" + str(tracker.getViewpoint(userId)), 
            print ";User (display projection)  [px]: " + str(trackerController.getViewpointProjection(userId)),
        print
        
if __name__ == '__main__':
    main()
