import time
from KinectTracker.ViewpointTrackerKinect import ViewpointTrackerKinect

def main(): 
    k = ViewpointTrackerKinect(libraryPath="lib/libSkeletonTrackerKinect.so")
    time.sleep(4)

    while(1):
        for id in range(k.getNumberOfUsers()):
            print id," ", k.getViewpoint(id)
        time.sleep(0.1)
        
if __name__ == '__main__':
    main()