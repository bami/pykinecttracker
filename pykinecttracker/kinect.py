# -*- coding: utf-8 -*-
"""
    pykinectracker.kinect
    ~~~~~~

    :copyright: (c) 2010,2011 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: Python wrapper of C++ Viewpoint tracker
"""

from ctypes import *
import time
from pykinecttracker.core import  ViewpointTracker

class ViewpointTrackerKinect(ViewpointTracker):
    ''' Python wrapper class for viewpoint tracker '''
    
    def __init__(self,libraryPath,**kwargs):
        ''' initialize
        
        arguments
            libraryPath - path string of c library 
        '''
        
        super(ViewpointTrackerKinect,self).__init__(**kwargs)
        
        ViewpointTrackerKinect._trackerLib = cdll.LoadLibrary(libraryPath)
        self.obj = ViewpointTrackerKinect._trackerLib.ViewpointTracker_instance()
        
    def setKinectTiltAngle(self,angle):
        ViewpointTrackerKinect._trackerLib.ViewpointTracker_setKinectTiltAngle(self.obj,angle)
        
    def getKinectOrientation(self):
        ''' return the kinect orientation
            
        return
            orientation - [x,y,z]
        '''
        x, y, z = c_double(),c_double(),c_double()
        ViewpointTrackerKinect._trackerLib.ViewpointTracker_getKinectOrientation(self.obj,
                                                         byref(x),
                                                         byref(y),
                                                         byref(z))
        orientation = [x.value,y.value,z.value]
        return orientation
    
    def wipeRepository(self):
        ''' wipe the tracker repository '''
        ViewpointTrackerKinect._trackerLib.ViewpointTracker_wipeRepository(self.obj)
        
    def printRepository(self):
        ''' wipe the tracker repository '''
        ViewpointTrackerKinect._trackerLib.ViewpointTracker_printRepository(self.obj)
    
    def getViewpoint(self,id=0):
        ''' return the viewpoint with ID id
        
        arguments
            id - viewpoint id [int]
            
        return
            viewpoint - [x,y,z]
        '''
        x, y, z = c_double(),c_double(),c_double()
        status = ViewpointTrackerKinect._trackerLib.ViewpointTracker_getViewpoint(self.obj, 
                                                 id, 
                                                 byref(x),
                                                 byref(y),
                                                 byref(z))
        if (status < 0):
            raise Exception("Error gathering viewpoint")
    
        viewpoint = [x.value,y.value,z.value]
        return viewpoint
    
    def getNumberOfUsers(self):
        ''' return the number of detected users '''
        return ViewpointTrackerKinect._trackerLib.ViewpointTracker_getNumerOfUsers(self.obj)
    
    def getSmoothingFactor(self):
        """
        return smoothing factor
        """
        alpha = c_double()
        ViewpointTrackerKinect._trackerLib.ViewpointTracker_getSmoothing(self.obj,byref(alpha))
        return alpha.value
    
    def setSmoothingFactor(self, alpha):
        """
        set factor of exponential smoothing
        
        arguments:
            alpha - smoothing factor [0,1] (1 = no smoothing)
        """
        
        return ViewpointTrackerKinect._trackerLib.ViewpointTracker_setSmoothing(self.obj, c_double(alpha))
        


def main(): 
    k = ViewpointTrackerKinect(libraryPath="lib/libSkeletonTrackerKinect.so")
    time.sleep(4)

    while(1):
        for id in range(k.getNumberOfUsers()):
            print id," ", k.getViewpoint(id)
        time.sleep(0.1)

if __name__ == '__main__':
    main()
    
