# -*- coding: utf-8 -*-
"""
    pykinectracker.core
    ~~~~~~

    :copyright: (c) 2010,2011 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
"""
import pykinecttracker.utils as utils

class ViewpointTracker(object):
    """
    Viewpoint tracker
    """
    
    def __init__(self):
        pass
    
    def getViewpoint(self,id=0):
        """
        return user viewpoint position as 3d python list
        """
        raise NotImplementedError()
    
    def getNumberOfUsers(self):
        """
        return the number of tracked users
        """
        raise NotImplementedError()
    
    def transformTrackingToDisplayCoordinates(self,viewpoint,
                                              camera_position = [0,0,0],
                                              pixelpitch = [1.,1.]):
        '''
        transform viewpoint in tracking coordinates (in [mm] and camera 
        position) into display coordinates [px]

        transformation is translation and scaling only, no rotation!!

        Arguments
            viewpoint - viewpoint coordinates [x,y,z] in camera coordinate system
            cameraPosition - camera coordinates [x,y,z] in display coordinate system [mm]
            pixel_pitch - pixel size in [mm]
        '''

        viewpoint[1] = -viewpoint[1] # swap y axis
        temp = [t+c for t,c in zip(viewpoint,camera_position)] # move to display origin
        viewpointDisplay = utils.mm2px(pixelpitch,temp)
        return viewpointDisplay