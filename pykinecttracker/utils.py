# -*- coding: utf-8 -*-
"""
    pykinectracker.utils
    ~~~~~~

    :copyright: (c) 2010,2011 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: helper functions
"""


def px2mm(pixel_pitch, display_coordinates):
    """
    transform display coordinates (2d) from [px] to [mm]
    """
    return [ v * p for v,p in zip(display_coordinates,pixel_pitch)]


def mm2px(pixel_pitch, display_coordinates):
    """
    transform display coordinates (2d) from [mm] to [px]
    """
    return [ v / p for v,p in zip(display_coordinates,pixel_pitch)]