# -*- coding: utf-8 -*-
"""
    pykinectracker.dummy
    ~~~~~~

    :copyright: (c) 2012 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: dummy viewpoint tracker
"""

from threading import Thread
import random 
from pykinecttracker.core import ViewpointTracker

class RandomViewpointTracker(ViewpointTracker, Thread):
    '''
    dummy / random viewpoint locator
    
    random viewpoint between 0 and maxValue
    '''
    
    def __init__(self,**kwargs):
        ''' init
        
        arguments
            maxValue - upper bound of position
            
        '''
        
        self.position = [0,0,0]
        self.maxValue = kwargs['maxValue']
        del kwargs['maxValue']
        
        super(RandomViewpointTracker,self).__init__(**kwargs)

        
        return
    

    def getViewpoint(self,id=0):
        """
        update and return new viewpoint position
        
        arguments
            id -- fake viewpoint id
        """
        for dimIndex in range(3):
            self.position[dimIndex] = int(random.random()*self.maxValue)
        return self.position
    
    def getNumberOfUsers(self):
        return 1
    
class StaticViewpointTracker(ViewpointTracker):
    """
    viewpoint tracker with static viewpoint
    """
    
    def __init__(self,*args,**kwargs):
        """
        initialize tracker

        arguments
            position -- 3d viewpoint position as python list
        """
        
        self.position = kwargs['position']
        del kwargs['position']
        
        super(StaticViewpointTracker,self).__init__(*args,**kwargs)
        
    def getViewpoint(self,id=0):
        return self.position
    
    def getNumberOfUsers(self):
        return 1