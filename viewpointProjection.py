# -*- coding: utf-8 -*-
"""
    pyqtpc.viewpointProjection
    ~~~~~~

    :copyright: (c) 2012 by Bastian Migge and Oliver Schlatter
    :license: BSD3, see LICENSE for more details.
"""

import sys
import time
from PyQt4 import QtGui, QtCore
from threading import Thread
from pyqtpc.Correction.Controller.Tracking import ViewpointTrackerCorrectionController
from pykinecttracker.ViewpointTrackerKinect import ViewpointTrackerKinect

class ViewpointProjectionWidget(QtGui.QWidget,Thread):
    '''
        Qt4 Point widget: shows a point at x,y
    '''
         
    def __init__(self,controller):
        ''' initialize
        
        Arguments
            controller - ViewpointTrackerCorrectionController
        '''
        
        Thread.__init__(self)
        super(SketchWidget, self).__init__()
        
        
        self.initUI()
        
        self._x,self._y = -1,-1
        self.radius = 10
        
        self.paintSwitch = 0
        self.pointBuffer = []

        self.pen = QtGui.QPen(QtCore.Qt.SolidLine)
        self.pen.setColor(QtCore.Qt.black)
        self.pen.setWidth(2)
        
        self.brush = QtGui.QBrush(QtCore.Qt.yellow)
        
        self.controller = controller
        
        self.start()
                
    def initUI(self):      

        self.text = 'drawing app'
        self.setWindowTitle(self.__class__.__name__)

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        
        qp.begin(self)
        qp.setPen(self.pen)
        for userId in range(self.controller.viewpointTracker.getNumberOfUsers()):
            [x,y] = self.controller.getViewpointProjection(userId)
            qp.setBrush(self.brush)  
            qp.drawEllipse(int(x)-self.radius, int(y)-self.radius, 2*self.radius, 2*self.radius)
            
        qp.end()      

    def run(self):
        while(1):
            self.update()
            time.sleep(0.1)

    def mousePressEvent(self, event):
        self.update()
    
    def mouseMoveEvent(self, event):
        self.update()
        
        
                
def main():    

    tracker = ViewpointTrackerKinect(libraryPath="./lib/libSkeletonTrackerKinect.so")
    tracker.setSmoothingFactor(0.2)
    correctionController = ViewpointTrackerCorrectionController(viewpointTracker=tracker,
                                                      displayOffset = 50,
                                                      pixelPitch = [0.255,0.255],
                                                      cameraPosition = [206., -226., -225.])
    
    
    
    app = QtGui.QApplication(sys.argv)
    
    w = SketchWidget(correctionController)
    w.showMaximized()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
